﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Troya_Workflow_Backend.Helpers
{
    public class LoginHelper
    {


        public string HashPassword(string passsword)
        {
            try
            {
                return BCrypt.Net.BCrypt.HashPassword(passsword);
            }
            catch (Exception)
            {

                return passsword;
            }

        }

        public bool ValidatePassword(string NormalPassword, string HashingPassword)
        {
            try
            {
                bool isValidPassword = BCrypt.Net.BCrypt.Verify(NormalPassword, HashingPassword);
                if (isValidPassword)
                {
                    return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }

        }
    }
}
