﻿using AutoMapper;
using Troya_Workflow_Backend.Core.Entities;
using Troya_Workflow_Backend.DTOS.WorkflowDTOS;

namespace Troya_Workflow_Backend.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<WorkflowEntity, WorkflowDTO>().ReverseMap();
            CreateMap<WorkflowEntity, CreateWorkflowDTO>().ReverseMap();

        }
    }
}
