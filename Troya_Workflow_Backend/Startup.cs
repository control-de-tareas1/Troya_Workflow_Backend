using System;
using System.Text;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Troya_Workflow_Backend.Core;
using Troya_Workflow_Backend.Core.ContextMongoDB;
using Troya_Workflow_Backend.Repository;
using Troya_Workflow_Backend.Repository.UserCollection;


namespace Troya_Workflow_Backend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper(typeof(Startup));

            services.Configure<MongoSettings>(

               options =>
               {
                   options.Database = Configuration.GetSection("Mongodb:Database").Value;
                   options.ConnectionString  = Configuration.GetValue<string>("ConnectionStringMongoTroya");
               }
            );
            services.AddSingleton<MongoSettings>();


            // Ocupamos transiente para cada transaccion individual que se vaya ejecutando 
            services.AddTransient<IUsuarioContext, UsuarioContext>();

            services.AddTransient<IUserRepository, UserRepository>();

            // Inicia en el request y termina en el response - garbage collector 

            services.AddScoped(typeof(IMongoRepository<>), typeof(MongoRepository<>));

            services.AddControllers();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options => 
            {
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(
                        Encoding.UTF8.GetBytes(Configuration["JWT:ClaveSecreta"])
                    )
                };
            });


            AddSwagger(services);


            services.AddCors(opt =>
            {
                opt.AddPolicy("CorsRules", rule =>
                {
                    rule.AllowAnyHeader().AllowAnyMethod().WithOrigins("*");
                });
            });
        }

   
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Troya Workflow API V1");
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseCors("CorsRules");


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void AddSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                var groupName = "v1";

                options.SwaggerDoc(groupName, new OpenApiInfo
                {
                    Title = $"TroyaWorkflowAPI {groupName}",
                    Version = groupName,
                    Description = "API de consumo de flujos de tareas",
                    Contact = new OpenApiContact
                    {
                        Name = "Acropolis",
                        Email = string.Empty,
                        Url = new Uri("https://foo.com/"),
                    }
                });
            });


        }


    }
}
