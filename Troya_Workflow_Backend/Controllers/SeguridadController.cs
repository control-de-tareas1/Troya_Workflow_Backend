﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

using Troya_Workflow_Backend.Core.Entities;
using Troya_Workflow_Backend.DTOS.UserDTOS;
using Troya_Workflow_Backend.Helpers;
using Troya_Workflow_Backend.Repository.UserCollection;

namespace Troya_Workflow_Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SeguridadController : ControllerBase
    {
        private readonly IUserRepository userRepository;
        private readonly IConfiguration configuration;

        public SeguridadController(IUserRepository _userRepository, IConfiguration configuration)
        {
            this.userRepository = _userRepository;
            this.configuration = configuration;
        }

        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login(UserLoginDTO usuarioLogin)
        {

            var _userInfo = await AutenticarUsuarioAsync(usuarioLogin.Email, usuarioLogin.Password);
            if (_userInfo != null)
            {
                foreach (string privilegio in _userInfo.Roles.Privilegios)
                {
                    if (privilegio == "Acceso Total Flujo de Trabajo")  
                        return Ok(new { token = GenerarTokenJWT(_userInfo) });                 
                }
                
                return Unauthorized("No tienes Acceso");
            }
            else
            {
                return NotFound("Usuario no encontrado");
            }
        }

        private async Task<Usuario> AutenticarUsuarioAsync(string mail, string password) 
        {

            try
            {
                LoginHelper loginHelper = new LoginHelper();
                Usuario usuario = await userRepository.Authentication(mail);

                if (usuario == null ) return null;              
                bool valid = loginHelper.ValidatePassword( password, usuario.Contrasena);


                if (valid == false) return null;
                return usuario;  

            }
            catch (Exception)
            {

                throw;
            }


        }

        private string GenerarTokenJWT(Usuario usuarioInfo)
        {
   
            var _symmetricSecurityKey = new SymmetricSecurityKey(
                    Encoding.UTF8.GetBytes(configuration["JWT:ClaveSecreta"])
                );
            var _signingCredentials = new SigningCredentials(
                    _symmetricSecurityKey, SecurityAlgorithms.HmacSha256
                );
            var _Header = new JwtHeader(_signingCredentials);

       
            var _Claims = new[] {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim("Developer", "nitro"),
                new Claim(JwtRegisteredClaimNames.Email, usuarioInfo.Correo),
            };
     
            var _Payload = new JwtPayload(
                    issuer: null,
                    audience: null,
                    claims: _Claims,
                    notBefore: DateTime.UtcNow,
                    expires: DateTime.UtcNow.AddDays(7)
                );

       
            var _Token = new JwtSecurityToken(
                    _Header,
                    _Payload
                );

            return new JwtSecurityTokenHandler().WriteToken(_Token);
        }





    }



}