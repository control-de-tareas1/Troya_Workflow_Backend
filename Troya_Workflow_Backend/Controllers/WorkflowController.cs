﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis;
using MongoDB.Driver;
using Troya_Workflow_Backend.Core.Entities;
using Troya_Workflow_Backend.DTOS.WorkflowDTOS;
using Troya_Workflow_Backend.Repository;

namespace Troya_Workflow_Backend.Controllers
{
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class WorkflowController : ControllerBase
    {


        private readonly IMongoRepository<WorkflowEntity> mongoRepositoryWorkflow;
        private readonly IMongoRepository<TaskEntity> mongoRepositoryTask;
        private readonly IMapper mapper;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="_mongoRepositoryWorkflow"></param>
        /// <param name="_mongoRepositoryTask"></param>
        /// <param name="_mapper"></param>
        public WorkflowController(IMongoRepository<WorkflowEntity> _mongoRepositoryWorkflow,
                                  IMongoRepository<TaskEntity> _mongoRepositoryTask,
                                  IMapper _mapper
                                  )
        {
            this.mongoRepositoryWorkflow = _mongoRepositoryWorkflow;

            this.mongoRepositoryTask = _mongoRepositoryTask;
            this.mapper = _mapper;
         
        }


        #region Workflow

        /// <summary>
        /// Retonar todos los Workflow
        /// </summary>
        /// <returns></returns>
        [HttpGet]

        public async Task<ActionResult<List<WorkflowDTO>>> GetAllWorkflows()
        {
            

            var workflows = await mongoRepositoryWorkflow.Getall();
            var workflowDTOs = mapper.Map<List<WorkflowDTO>>(workflows); 

            if (workflowDTOs.Count() > 0)              
                return Ok(workflowDTOs);
            else
                return null;
        }


        /// <summary>
        /// Retornar Workflow
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        [HttpGet("{id}")]

        public async Task<ActionResult<WorkflowDTO>> GetWorkflowById(string id)
        {

            var workflow = await mongoRepositoryWorkflow.GetById(id);

            if (workflow == null)
                return NotFound();

            var workflowDTO = mapper.Map<List<WorkflowDTO>>(workflow);
            return Ok(workflowDTO);

        }


        /// <summary>
        /// Crear Workflow
        /// </summary>
        /// <param name="workflowDTO"></param>
        /// <returns></returns>

        [HttpPost]
        public async Task<ActionResult<WorkflowEntity>> PostWorkflow(CreateWorkflowDTO workflowDTO) 
        {
            var workflowEntity = mapper.Map<WorkflowEntity>(workflowDTO);
            var workflow = await mongoRepositoryWorkflow.InsertDocument(workflowEntity);
            if (workflow == null)
                return NotFound();
            return Ok(workflow);

        }

        /// <summary>
        /// Elimina un Workflow 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        

        [HttpDelete("{id}")]

        public async Task DeleteWorkflow(string id)
        {
            await mongoRepositoryWorkflow.DeleteById(id);
            if ( id!=null){

                var filter = Builders<TaskEntity>.Filter.Eq(s => s.IdWorkflow, id);
                await mongoRepositoryTask.DeleteMany(filter);

            }
        }

        /// <summary>
        /// Actualiza un Workflow
        /// </summary>
        /// <param name="workflowEntity"></param>
        /// <returns></returns>
        
        [HttpPut]

        public async Task PutWorkflow(WorkflowEntity workflowEntity)
        {
            await mongoRepositoryWorkflow.UpdateDocument(workflowEntity);
        }

        [HttpPost("pagination")]

        public async Task<ActionResult<PaginationEntity<WorkflowDTO>>> PostPagination(PaginationEntity<WorkflowEntity> pagination)
        {
            var resultados = await mongoRepositoryWorkflow.PaginationByfilter(
                pagination
                );

            return Ok(resultados);
        }  


        #endregion

        #region Tasks


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="taskEntity"></param>
        /// <returns></returns>

        [HttpPost("{id}/tasks")]

        public async Task<ActionResult<TaskEntity>> PostTask(string id, TaskEntity taskEntity)
        {

            var workflow = await mongoRepositoryWorkflow.GetById(id);
            if (workflow == null)
                // la lista no existe
                return NotFound();

            taskEntity.IdWorkflow = id;

            var task = await mongoRepositoryTask.InsertDocument(taskEntity);
            if (task == null)
                return NotFound();
            return Ok(task);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 
        [HttpGet("{id}/tasks")]
        public async Task<ActionResult<IEnumerable<TaskEntity>>> GetAllTaskFromWorkFlow(string id)
        {

            var filter = Builders<TaskEntity>.Filter.Eq(s => s.IdWorkflow, id);
            var tasksfromList = await mongoRepositoryTask.GetallbById(filter);
            return Ok(tasksfromList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        [HttpDelete("{id}/task")]
        public async Task DeleteTask(string id)
        {
            await mongoRepositoryTask.DeleteById(id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        [HttpDelete("{id}/taskAll")]
        public async Task DeleteTaskAll(string id)
        {
            var filter = Builders<TaskEntity>.Filter.Eq(s => s.IdWorkflow, id);
            await mongoRepositoryTask.DeleteMany(filter);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="taskEntity"></param>
        /// <returns></returns>
        [HttpPut("task")]
        public async Task UpdateTask(TaskEntity taskEntity)
        {
            await mongoRepositoryTask.UpdateDocument(taskEntity);
        }

        #endregion


        

    }
}