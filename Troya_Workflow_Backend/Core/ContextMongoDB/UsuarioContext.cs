﻿using Microsoft.Azure.KeyVault;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Troya_Workflow_Backend.Core.Entities;

namespace Troya_Workflow_Backend.Core.ContextMongoDB
{
    public class UsuarioContext : IUsuarioContext
    {
        private readonly IMongoDatabase _db;

        public UsuarioContext(IOptions<MongoSettings> options )
        {
          

            var client = new MongoClient(options.Value.ConnectionString);
            _db = client.GetDatabase(options.Value.Database); 
        }
      
        public IMongoCollection<Usuario> Usuarios => _db.GetCollection<Usuario>("usuarios");
    
    }
}
