﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Troya_Workflow_Backend.Core.Entities
{
    public class Usuario
    {
        public Usuario(ObjectId id, string nombre, string apellido, string contrasena, string correo, string departamento, Roles roles)
        {
            Id = id;
            Nombre = nombre;
            Apellido = apellido;
            Contrasena = contrasena;
            Correo = correo;
            Departamento = departamento;
            Roles = roles;
        }

        public Usuario()
        {

        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId Id { get; set; }

        [BsonElement("NombreUsuario")]
        public string Nombre { get; set; }

        [BsonElement("ApellidoUsuario")]
        public string Apellido { get; set; }

        [BsonElement("ContrasenaUsuario")]
        public string Contrasena { get; set; }

        [BsonElement("CorreoUsuario")]
        public string Correo { get; set; }

        [BsonElement("DepartamentoUsuario")]
        public string Departamento { get; set; }

        [BsonElement("Roles")]
        public Roles Roles { get; set; }
    }
}
