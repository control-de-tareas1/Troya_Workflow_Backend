﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Troya_Workflow_Backend.Core.Entities
{
    public class RolEntity : Document
    {


        [BsonElement("NombreRol")]
        public string NombreRol { get; set; }

        [BsonElement("Privilegios")]
        public ArrayList Privilegios { get; set; }



    }
}
