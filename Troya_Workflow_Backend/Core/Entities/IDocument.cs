﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Troya_Workflow_Backend.Core.Entities
{
    public interface IDocument
    {
        // Propiedades genéricas 


        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        string Id { get; set; }

    }
}
