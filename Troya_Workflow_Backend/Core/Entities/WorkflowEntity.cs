﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Troya_Workflow_Backend.Core.Entities
{

    [BsonCollection("workflows")]

    public class WorkflowEntity : Document
    {

        [BsonElement("NombreFlujo")]
        public string Nombre { get; set; }

    }
}
