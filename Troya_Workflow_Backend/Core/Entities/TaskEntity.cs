﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Troya_Workflow_Backend.Core.Entities
{

    [BsonCollection("tareasWorkflow")]

    public class TaskEntity : Document
    {

        [BsonElement("NombreTarea")]
        public string Nombre  { get; set; }

        [BsonElement("IdWorkflow")]
        public string IdWorkflow { get; set; }

    }
}
