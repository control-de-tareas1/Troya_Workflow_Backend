﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Troya_Workflow_Backend.Core.Entities
{

    [BsonCollection("usuarios")]
    public class UserEntity : Document
    {

        [BsonElement("NombreUsuario")]
        public string Nombre { get; set; }

        [BsonElement("ApellidoUsuario")]
        public string Apellido { get; set; }

        [BsonElement("ContrasenaUsuario")]
        public string Contrasena { get; set; }

        [BsonElement("CorreoUsuario")]
        public string Correo { get; set; }

        [BsonElement("DepartamentoUsuario")]
        public string Departamento { get; set; }

        [BsonElement("Roles")]
        public RolEntity Roles { get; set; }

    }
}
