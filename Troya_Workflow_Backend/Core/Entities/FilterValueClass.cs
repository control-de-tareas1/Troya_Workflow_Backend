﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Troya_Workflow_Backend.Core.Entities
{
    public class FilterValueClass
    {
        public string Propiedad { get; set; }

        public string Valor { get; set; } 

    }

}
