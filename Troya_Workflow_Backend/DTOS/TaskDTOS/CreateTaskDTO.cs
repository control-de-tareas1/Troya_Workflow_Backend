﻿using MongoDB.Bson.Serialization.Attributes;
using Troya_Workflow_Backend.Core.Entities;

namespace Troya_Workflow_Backend.DTOS.TaskDTOS
{

    [BsonCollection("tareasWorkflow")]

    public class CreateTaskDTO
    {
        [BsonElement("NombreTarea")]
        public string Nombre { get; set; }

        [BsonElement("IdWorkflow")]
        public string IdWorkflow { get; set; }
    }
}
