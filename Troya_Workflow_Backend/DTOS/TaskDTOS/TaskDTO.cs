﻿using MongoDB.Bson.Serialization.Attributes;
using Troya_Workflow_Backend.Core.Entities;
using Document = Troya_Workflow_Backend.Core.Entities.Document;

namespace Troya_Workflow_Backend.DTOS.TaskDTOS
{

    [BsonCollection("tareasWorkflow")]

    public class TaskDTO : Document 
    {

        [BsonElement("NombreTarea")]
        public string Nombre { get; set; }

        [BsonElement("IdWorkflow")]
        public string IdWorkflow { get; set; }

    }
}
