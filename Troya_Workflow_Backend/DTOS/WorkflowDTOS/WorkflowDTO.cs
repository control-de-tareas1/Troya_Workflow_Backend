﻿using Microsoft.CodeAnalysis;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Troya_Workflow_Backend.Core.Entities;
using Document = Troya_Workflow_Backend.Core.Entities.Document;

namespace Troya_Workflow_Backend.DTOS.WorkflowDTOS
{

    [BsonCollection("workflows")]

    public class WorkflowDTO  : Document
    {

        [BsonElement("NombreFlujo")]
        public string Nombre { get; set; }

    }
}
