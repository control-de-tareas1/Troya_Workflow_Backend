﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Troya_Workflow_Backend.Core.Entities;

namespace Troya_Workflow_Backend.DTOS.WorkflowDTOS
{


    [BsonCollection("workflows")]
    public class CreateWorkflowDTO
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; } 

        [BsonElement("NombreFlujo")]
        public string Nombre { get; set; }
    }
}
