﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Troya_Workflow_Backend.DTOS.UserDTOS
{
    public class UserLoginDTO
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }
}
