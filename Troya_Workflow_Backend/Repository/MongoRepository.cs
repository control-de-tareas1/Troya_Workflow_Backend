﻿using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Troya_Workflow_Backend.Core;
using Troya_Workflow_Backend.Core.Entities;

namespace Troya_Workflow_Backend.Repository
{
    public class MongoRepository<TDocument> : IMongoRepository<TDocument> where TDocument : IDocument
    {

        private readonly IMongoCollection<TDocument> mongoCollection;

        public MongoRepository( IOptions<MongoSettings> options)
        {
            var client = new MongoClient(options.Value.ConnectionString);
            var _db = client.GetDatabase(options.Value.Database);
            mongoCollection = _db.GetCollection<TDocument>(GetCollectionName(typeof(TDocument)));
        }

        private protected string GetCollectionName(Type DocumentType)
        {
            return ((BsonCollectionAttribute)DocumentType
                .GetCustomAttributes(typeof(BsonCollectionAttribute), true).FirstOrDefault()).CollectionName;
        }
        public async Task<IEnumerable<TDocument>>  Getall()
        {
           return await mongoCollection.Find(p => true).ToListAsync();
        }

        public async Task<IEnumerable<TDocument>> GetallbById(FilterDefinition<TDocument> filter)
        {

            return await mongoCollection.Find(filter).ToListAsync();
        }

        public async Task<TDocument> GetById(string Id)
        {
            var filter = Builders<TDocument>.Filter.Eq(s => s.Id, Id);

            return await mongoCollection.Find(filter).SingleOrDefaultAsync();

        }

        public async Task<TDocument> InsertDocument(TDocument document)
        {
            var flujo = document; 
            await mongoCollection.InsertOneAsync(document);
            return flujo;
        }

        public async Task UpdateDocument(TDocument document)
        {
            var filter = Builders<TDocument>.Filter.Eq(doc => doc.Id, document.Id);
            await mongoCollection.FindOneAndReplaceAsync(filter, document);
        }

        public async Task DeleteById(string Id)
        {

            var filter = Builders<TDocument>.Filter.Eq(s => s.Id, Id);
            await mongoCollection.FindOneAndDeleteAsync(filter);

        }

        public async Task DeleteMany(FilterDefinition<TDocument> filter)
        {

            await mongoCollection.DeleteManyAsync(filter);

        }

        public async Task<PaginationEntity<TDocument>> PaginatioBy (

            Expression<Func<TDocument, bool>> filterExpression, 
            PaginationEntity<TDocument> pagination)
        {
            var sort = Builders<TDocument>.Sort.Ascending(pagination.Sort);
            if( pagination.SortDirection == "desc"){
                sort = Builders<TDocument>.Sort.Descending(pagination.Sort);
            }

            if( string.IsNullOrEmpty(pagination.Filter)){

                pagination.Data = await mongoCollection.Find(p => true)
                                    .Sort(sort)
                                    .Skip((pagination.Page -1) * pagination.PageSize)
                                    .Limit(pagination.PageSize)
                                    .ToListAsync();
            }
            else{

                pagination.Data = await mongoCollection.Find(filterExpression)
                    .Sort(sort)
                    .Skip((pagination.Page - 1) * pagination.PageSize)
                    .Limit(pagination.PageSize)
                    .ToListAsync();
            }

            long totalDocument = await mongoCollection.CountDocumentsAsync(FilterDefinition<TDocument>.Empty);
            var totalPages = Convert.ToInt32(Math.Ceiling( Convert.ToDecimal( totalDocument / pagination.PageSize)));

            pagination.PagesQuantity = totalPages;
            return pagination;
        
            
        }

        public async Task<PaginationEntity<TDocument>> PaginationByfilter(PaginationEntity<TDocument> pagination)
        {
            var sort = Builders<TDocument>.Sort.Ascending(pagination.Sort);
            if (pagination.SortDirection == "desc")
            {
                sort = Builders<TDocument>.Sort.Descending(pagination.Sort);
            }

            var totalDocument = 0;  

            if (pagination.FilterValue == null)
            {

                pagination.Data = await mongoCollection.Find(p => true)
                                    .Sort(sort)
                                    .Skip((pagination.Page - 1) * pagination.PageSize)
                                    .Limit(pagination.PageSize)
                                    .ToListAsync();

                totalDocument =  (await mongoCollection.Find(p => true).ToListAsync()).Count();
            }
            else
            {
                var valueFilter = ".*" + pagination.FilterValue.Valor + ".*";
                var filter = Builders<TDocument>.Filter.Regex(pagination.FilterValue.Propiedad, new BsonRegularExpression(valueFilter, "i"));

                pagination.Data = await mongoCollection.Find(filter)
                    .Sort(sort)
                    .Skip((pagination.Page - 1) * pagination.PageSize)
                    .Limit(pagination.PageSize)
                    .ToListAsync();

                totalDocument = (await mongoCollection.Find(filter).ToListAsync()).Count();
            }

            //long totalDocument = await mongoCollection.CountDocumentsAsync(FilterDefinition<TDocument>.Empty);
            var rounded  = Math.Ceiling(totalDocument / Convert.ToDecimal(pagination.PageSize)); 
            var totalPages = Convert.ToInt32(rounded);

            pagination.PagesQuantity = totalPages;
            pagination.TotalRows = Convert.ToInt32(totalDocument);
            return pagination;
        }
    }
}
