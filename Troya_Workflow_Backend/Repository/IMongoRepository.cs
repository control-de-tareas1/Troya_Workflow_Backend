﻿using Microsoft.CodeAnalysis;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Troya_Workflow_Backend.Core.Entities;


namespace Troya_Workflow_Backend.Repository
{

    /// <summary>
    /// La interface generica solo va a procesar lo que hereden de IDocument 
    /// </summary>
    /// <typeparam name="TDocument"></typeparam>
    public interface IMongoRepository<TDocument> where TDocument : IDocument
    {
        Task<IEnumerable<TDocument>> Getall();

        Task<TDocument> GetById(string Id);

        Task<TDocument> InsertDocument(TDocument document);

        Task UpdateDocument(TDocument document);

        Task DeleteById(string Id);

        Task DeleteMany(FilterDefinition<TDocument> filter);

        Task<IEnumerable<TDocument>> GetallbById(FilterDefinition<TDocument> filter);

        Task<PaginationEntity<TDocument>> PaginatioBy(
            Expression<Func<TDocument,bool>> filterExpression,
            PaginationEntity<TDocument> pagination
        );

        Task<PaginationEntity<TDocument>> PaginationByfilter( 
            PaginationEntity<TDocument> pagination); 
  
    }
}
