﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Troya_Workflow_Backend.Core.Entities;

namespace Troya_Workflow_Backend.Repository.UserCollection
{
    public interface IUserRepository
    {

        Task<Usuario> Authentication(string mail);
    }
}
