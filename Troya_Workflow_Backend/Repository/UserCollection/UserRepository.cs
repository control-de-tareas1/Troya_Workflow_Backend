﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Troya_Workflow_Backend.Core.ContextMongoDB;
using Troya_Workflow_Backend.Core.Entities;

namespace Troya_Workflow_Backend.Repository.UserCollection
{
    public class UserRepository : IUserRepository
    {

        private readonly IUsuarioContext usuarioContext;
            
        public UserRepository(IUsuarioContext _userContext)
        {

            this.usuarioContext = _userContext;

        }

        public async Task<Usuario> Authentication(string mail)
        {
            var filter = Builders<Usuario>.Filter.Where(p => p.Correo == mail);
            return await usuarioContext.Usuarios.FindAsync(filter).Result.FirstOrDefaultAsync();
        }

    }
}
