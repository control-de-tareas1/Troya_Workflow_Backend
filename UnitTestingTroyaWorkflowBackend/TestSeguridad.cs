﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Troya_Workflow_Backend.Controllers;
using Troya_Workflow_Backend.DTOS.UserDTOS;
using Troya_Workflow_Backend.Repository.UserCollection;

namespace UnitTestingTroyaWorkflowBackend
{

    [TestClass]
    public class TestSeguridad
    {
        private readonly IConfiguration configuration;

        [TestMethod]
        public void TestNotLogin()
        {

            // Arrange
            Mock<IUserRepository> userMock = new Mock<IUserRepository>();
            SeguridadController seguridadController = new SeguridadController(userMock.Object, configuration);

            UserLoginDTO userLoginDTO = new UserLoginDTO();
            userLoginDTO.Email = "master@gmail.com";
            userLoginDTO.Password = "1234";

            // Act
            var resultado = seguridadController.Login(userLoginDTO);

            //Assert
            Assert.IsNotNull(resultado);
        }

    }
}
