﻿using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Driver;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Troya_Workflow_Backend.Core;
using Troya_Workflow_Backend.Core.ContextMongoDB;
using Troya_Workflow_Backend.Core.Entities;
using Troya_Workflow_Backend.Repository;

namespace UnitTestingTroyaWorkflowBackend
{

    [TestClass]
    public class TestConnectionMongo
    {

        private Mock<IOptions<MongoSettings>> _mockOptions;
        private Mock<IMongoDatabase> _mockDB;
        private Mock<IMongoClient> _mockClient;


        public TestConnectionMongo()
        {
            _mockOptions = new Mock<IOptions<MongoSettings>>();
            _mockDB = new Mock<IMongoDatabase>();
            _mockClient = new Mock<IMongoClient>();
        }

        [TestMethod]
        public void MongoContext()
        {
            var settings = new MongoSettings()
            {
                ConnectionString = "mongodb+srv://cristopher:cristopher2020@troya.gnddo.gcp.mongodb.net/",
                Database = "ControlTareas"
            };

            _mockOptions.Setup(s => s.Value).Returns(settings);
            _mockClient.Setup(c => c.GetDatabase(_mockOptions.Object.Value.Database, null));

            var context = new UsuarioContext(_mockOptions.Object);

            Assert.IsNotNull(context);



        }

    }
}
